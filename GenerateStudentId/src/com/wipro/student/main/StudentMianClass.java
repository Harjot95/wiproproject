package com.wipro.student.main;

import java.util.Scanner;

import com.wipro.student.basic.Student;
import com.wipro.student.service.StudentIDGenerator;

public class StudentMianClass {

	public static void main(String[] args) {
		
		Student student = new Student();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the First Name of the student : ");
		student.setFirstName(scanner.nextLine());
		
		System.out.println("Enter the Last Name of the student : ");
		student.setLastName(scanner.nextLine());
		
		System.out.println("Enter the Gender of the student : ");
		student.setGender(scanner.nextLine());
		
		System.out.println("Enter the Standard of the student : ");
		student.setStandard(scanner.nextInt());
		
		System.out.println("Enter the YearOfBirth of the student : ");
		student.setYearOfBirth(scanner.nextInt());
		
		
		System.out.println("Student ID Generated Is :: "+StudentIDGenerator.generateStudentID(student).toUpperCase());
		
		

	}

}
