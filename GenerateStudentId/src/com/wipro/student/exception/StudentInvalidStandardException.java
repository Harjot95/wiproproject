package com.wipro.student.exception;

@SuppressWarnings("serial")
public class StudentInvalidStandardException extends Exception {

	@Override
	public String toString() {
		return "Please Input Valid Standard.";
	}
	
	
}
