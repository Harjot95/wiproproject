package com.wipro.student.exception;

@SuppressWarnings("serial")
public class StudentFirstNameMaxLimitException extends Exception {

	@Override
	public String toString() {
		return "Student First Name Maximum Limit Exceeded.";
	}
	
	
}
