package com.wipro.student.exception;

@SuppressWarnings("serial")
public class StudentInvalidGenderException extends Exception {

	@Override
	public String toString() {
		return "Please Input Valid Gender.";
	}
	
	
}
