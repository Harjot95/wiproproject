package com.wipro.student.exception;

@SuppressWarnings("serial")
public class StudentInvalidYearOfBirthException extends Exception {

	@Override
	public String toString() {
		return "Please Input Valid Year Of Birth.";
	}
	
	
}
