package com.wipro.student.service;

import java.time.LocalDate;
import java.util.Random;

import com.wipro.student.basic.Student;
import com.wipro.student.exception.StudentFirstNameMaxLimitException;
import com.wipro.student.exception.StudentInvalidGenderException;
import com.wipro.student.exception.StudentInvalidStandardException;
import com.wipro.student.exception.StudentInvalidYearOfBirthException;
import com.wipro.student.exception.StudentLastNameMaxLimitException;

public class StudentIDGenerator {
	
	public static boolean validateStudentDetails(Student studentObject) throws Exception
	{	
		
		if(studentObject.getFirstName().length()>20)
		{
			throw new StudentFirstNameMaxLimitException();
		}
		
		if(studentObject.getLastName().length()>25)
		{
			throw new StudentLastNameMaxLimitException();
		}
		
		if(!(studentObject.getStandard()>=1 && studentObject.getStandard()<=10))
		{
			throw new StudentInvalidStandardException();
		}
		
		if(!(studentObject.getGender().equalsIgnoreCase("male") || studentObject.getGender().equalsIgnoreCase("female")))
		{
			throw new StudentInvalidGenderException();
		}
		
		if((LocalDate.now().getYear()-studentObject.getYearOfBirth())<3)
		{
			throw new StudentInvalidYearOfBirthException();
		}
		
		return true;
	}
	
	
	public static String generateStudentID(Student student)
	{
		String result=null;
		try {
			if(validateStudentDetails(student)==true)
			{
				Random random = new Random();
				String year=Integer.toString(student.getYearOfBirth());
				String rand=Integer.toString(random.nextInt(10000)+1000);
				StringBuffer str =new StringBuffer();
				 str.append(student.getFirstName().substring(0,2));
				 str.append(student.getLastName().substring(0,1));
				 str.append(student.getGender().substring(0,1));
				 str.append(year.substring(2,4));
				 str.append(rand.charAt(1));
				 str.append(rand.charAt(3));
				 result=str.toString();
			}
		} catch (Exception e) {
			System.out.println("Student ID Not Generated. Please Enter Proper Details");
			System.out.println(e.toString());
		}
		return result;
	}
	
	

}
